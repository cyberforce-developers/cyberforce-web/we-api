import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'
import RequestMethods from '@cyberforce/web-essentials/props/RequestMethods'
import RegistrationController from './registration'

import ExampleController from './example'

const routes: IApiRoute[] = [
  {
    uri: '/profile/all',
    method: RequestMethods.GET,
    auth: false,
    controller: ExampleController
  },
  {
    uri: '/profile/register',
    method: RequestMethods.POST,
    auth: false,
    controller: RegistrationController
  }
]

export default routes
