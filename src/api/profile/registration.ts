export default async (req: any, res: any) => {
  return res.status(200).json({
    data: {
      email: req.body.email
    }
  })
}