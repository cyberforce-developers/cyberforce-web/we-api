import INodes from '../app/intf/INodes'
import Api from '../app/props/Api'

const nodes: INodes = {
  all: [
    Api.Jobs,
    Api.Location,
    Api.Profile
  ],
  jobs: [
    Api.Jobs
  ],
  node2: [
    Api.Profile
  ],
  node3: [
    Api.Jobs
  ]
}

export default nodes
