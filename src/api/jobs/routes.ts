import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'
import RequestMethods from '@cyberforce/web-essentials/props/RequestMethods'

import ExampleController from './example'
import Example2Controller from './example2'

const routes: IApiRoute[] = [
  {
    uri: '/jobs/example',
    method: RequestMethods.GET,
    auth: false,
    controller: ExampleController
  },
  {
    uri: '/jobs/example2',
    method: RequestMethods.POST,
    auth: false,
    controller: Example2Controller
  }
]

export default routes
