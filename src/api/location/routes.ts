import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'
import RequestMethods from '@cyberforce/web-essentials/props/RequestMethods'

import ExampleController from './example'

const routes: IApiRoute[] = [
  {
    uri: '/locations',
    method: RequestMethods.GET,
    auth: false,
    controller: ExampleController
  },
  {
    uri: '/locations/add',
    method: RequestMethods.POST,
    auth: false,
    controller: ExampleController
  }
]

export default routes
