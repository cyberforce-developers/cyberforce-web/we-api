import jobs from '../../api/jobs/routes'
import profile from '../../api/profile/routes'
import location from '../../api/location/routes'

import IKeyRoutes from '../intf/IKeyRoutes'

const Api: IKeyRoutes = {
  Jobs: jobs,
  Profile: profile,
  Location: location
}

export default Api
