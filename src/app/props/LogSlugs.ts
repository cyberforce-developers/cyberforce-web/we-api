enum LogSlugs {
  server_start_no_config = 'server_start_no_config',
  server_bind_no_routes = 'server_bind_no_routes',
  server_bind_unknown_request = 'server_bind_unknown_request',
  app_sigterm_server_killed = 'app_sigterm_server_killed'
}

export default LogSlugs
