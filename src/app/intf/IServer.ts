import { Server } from 'http'
import IApiConfig from './IApiConfig'
import INodes from './INodes'

export default interface IServer {
  start(callback?: any): Server
}

export interface IServerConstructor {
  nodes: INodes
  config: IApiConfig
  node: string | undefined
  environment: string
}
