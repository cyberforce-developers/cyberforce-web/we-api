import IApiConfigEnvironment from './IApiConfigEnvironment'

export default interface IApiConfig {
  envs: {
    [key: string]: IApiConfigEnvironment
  }
}
