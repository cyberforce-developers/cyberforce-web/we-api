import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'

export default interface IKeyRoutes {
  [key: string]: IApiRoute[]
}
