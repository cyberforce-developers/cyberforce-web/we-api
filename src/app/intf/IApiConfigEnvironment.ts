export default interface IApiConfigEnvironment {
  server: {
    hostname: string
    port: number
    timeout: number
    cors: string[]
    headers: {
      allowCredentials: boolean
      allowMethods: string
      allowHeaders?: string
    }
  }
}
