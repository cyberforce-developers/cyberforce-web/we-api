import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'

export default interface INodes {
  all: IApiRoute[][]
  [key: string]: IApiRoute[][]
}
