import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'

export default interface IRouter {
  getAllRoutes(): IApiRoute[]
  getNodeRoutes(): IApiRoute[]
  getRoutes(): void
}
