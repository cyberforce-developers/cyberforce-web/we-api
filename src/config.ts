import IApiConfig from './app/intf/IApiConfig'
import Environments from '@cyberforce/web-essentials/props/Environments'

const config: IApiConfig = {
  envs: {}
}

config.envs[Environments.local] = {
  server: {
    hostname: 'localhost',
    port: 3100,
    timeout: 20000,
    cors: ['localhost'],
    headers: {
      allowCredentials: true,
      allowMethods: 'GET,PUT,POST,DELETE,OPTIONS',
      allowHeaders: 'Origin,Content-Type,Accept,Authorization'
    }
  }
}

config.envs[Environments.dev] = {
  server: {
    hostname: 'localhost',
    port: 80,
    timeout: 20000,
    cors: ['localhost'],
    headers: {
      allowCredentials: true,
      allowMethods: 'GET,PUT,POST,DELETE,OPTIONS',
      allowHeaders: 'Origin,Content-Type,Accept,Authorization'
    }
  }
}

export default config
