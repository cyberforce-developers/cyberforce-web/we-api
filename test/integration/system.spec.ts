import * as chai from 'chai'
import System from '../support/system'
import { Server } from 'http'

import IApiConfig from '../../src/app/intf/IApiConfig'
import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'
import INodes from '../../src/app/intf/INodes'

import nodes from '../support/app/nodes'
import * as routes from '../support/app/routes'

import realNodes from '../../src/api/nodes'
import Router from '../../dist/router'

const isPortReachable = require('is-port-reachable')   // no types
const expect = chai.expect

describe('Application (System)', () => {

  it('initialises a web server with a correct port', async () => {
    const config: IApiConfig = {
      envs: {
        testing: {
          server: {
            hostname: 'http://localhost',
            port: 6767,
            timeout: 20000,
            cors: ['localhost'],
            headers: {
              allowCredentials: true,
              allowMethods: 'GET,POST'
            }
          }
        }
      }
    }
    const { hostname, port } = config.envs.testing.server
    const system = new System({config})
    await system.start().then((server: Server) => server)
    const isPortOpen = await isPortReachable(port, { host: hostname.replace('http://', '') })
    expect(isPortOpen).to.be.true
    await system.stop()
  })

  it('initialises a web server with all test routes', async () => {
    const system = new System()
    await system.start().then((server: Server) => server)

    // Get all routes (test)
    const router = new Router(nodes, 'all')
    const allRoutes: IApiRoute[] = router.getAllRoutes()
    const allRoutesCheck = await Promise.all(
      allRoutes.map(async (route: IApiRoute) => await system.request({ route }))
    )
    allRoutesCheck.forEach((check: any) => {
      expect(check.status).equal(200)
    })
    await system.stop()
  })

  it('initialises a web server with all real routes', async () => {
    const system = new System({
      nodes: realNodes
    })
    await system.start().then((server: Server) => server)
    const router = new Router(realNodes, 'all')
    const allRoutes: IApiRoute[] = router.getAllRoutes()
    const allRoutesCheck = await Promise.all(
      allRoutes.map(async (route: IApiRoute) => await system.request({ route }))
    )
    allRoutesCheck.forEach((check: any) => {
      expect(check.status).equal(200)
    })
    await system.stop()
  })

  it('initialises a web server for a specific `node` on test routes', async () => {
    const separateNodes: INodes = {
      all: [],
      api1: [
        routes.location
      ],
      api2: [
        routes.profile
      ]
    }

    // Load all `api1` node routes only
    const system = new System({
      nodes: separateNodes,
      node: 'api1'
    })

    await system.start().then((server: Server) => server)
    const api1Router = new Router(separateNodes, 'api1')
    const api1Routes = api1Router.getNodeRoutes()
    const api2Router = new Router(separateNodes, 'api2')
    const api2Routes = api2Router.getNodeRoutes()

    // Check all initialised routes (api1)
    const api1RoutesCheck = await Promise.all(
      api1Routes.map(async (route: IApiRoute) => await system.request({ route }))
    )
    api1RoutesCheck.forEach((check: any) => {
      expect(check.status).equal(200)
    })

    // Check routes of the other API that shouldn't be included
    let api2RoutesCheck: string[] = []
    await Promise.all(
      api2Routes.map(async (route: IApiRoute) => {
        try {
          await system.request({ route })
        } catch {
          // 404
          api2RoutesCheck.push(route.uri)
        }
      })
    )
    expect(api2RoutesCheck.length).equal(api2Routes.length)
    await system.stop()
  })
})
