import { EventEmitter } from 'events'
import { Server } from 'http'
import IApiConfig from '../../src/app/intf/IApiConfig'
import INodes from '../../src/app/intf/INodes'
import { IWebRequestSendToApiRoute } from '@cyberforce/web-essentials/intf/modules/IWebRequest'

import CoreServer from '../../dist/server'
import WebRequest from '@cyberforce/web-essentials/modules/request/web'

import config from './config'
import nodes from './app/nodes'

interface ISystemArgs {
  nodes?: INodes
  node?: string
  config?: IApiConfig
}

class System {

  private server: CoreServer

  private express: Server

  private config: IApiConfig

  private nodes: INodes

  public constructor(args: ISystemArgs = {}) {
    this.config = (args.config)
      ? args.config
      : config

    this.nodes = (args.nodes)
      ? args.nodes
      : nodes

    this.server = new CoreServer({
      nodes: this.nodes,
      config: this.config,
      environment: 'testing',
      node: args.node
    })
  }

  public async start(): Promise<Server> {
    this.express = await this.startServer()
    return this.express
  }

  public async stop(): Promise<void> {
    if (this.express) {
      await this.express.close()
    }
  }

  public async request({ route, payload, options }: IWebRequestSendToApiRoute): Promise<any> {
    const settings = this.config.envs.testing.server
    const req = new WebRequest({
      baseUrl: `${settings.hostname}:${settings.port}`,
      timeout: settings.timeout,
    })
    return await req.sendToApiRoute({ route, payload, options })
  }

  private async startServer(): Promise<any> {
    let server: Server | undefined
    const serverReady = new EventEmitter()

    const unlockingServer = async () => {
      await new Promise((resolve: any) => {
        serverReady.once('serverListeningEvent', resolve)
      })
    }

    const callback = () => {
      serverReady.emit('serverListeningEvent')
    }
    server = this.server.start(callback)
    await unlockingServer()
    return server
  }

}
export default System
