import IApiConfig from '../../src/app/intf/IApiConfig'
import Environments from '@cyberforce/web-essentials/props/Environments'

const config: IApiConfig = {
  envs: {}
}

config.envs[Environments.testing] = {
  server: {
    hostname: 'http://localhost',
    port: 20666,
    timeout: 20000,
    cors: [],
    headers: {
      allowCredentials: true,
      allowMethods: 'POST,GET,OPTIONS'
    }
  }
}

export default config
