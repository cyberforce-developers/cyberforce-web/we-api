import INodes from '../../../src/app/intf/INodes'

import * as routes from './routes'

const nodes: INodes = {
  all: [
    routes.jobs,
    routes.location
  ],
  api1: [
    routes.location
  ],
  api2: [
    routes.profile
  ]
}

export default nodes
