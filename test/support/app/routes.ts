import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'
import RequestMethods from '@cyberforce/web-essentials/props/RequestMethods'
import testController from '../../../src/api/jobs/example'

export const jobs: IApiRoute[] = [
  {
    uri: '/jobs',
    method: RequestMethods.GET,
    auth: false,
    controller: testController
  },
  {
    uri: '/jobs/add',
    method: RequestMethods.POST,
    auth: false,
    controller: testController
  }
]

export const location: IApiRoute[] = [
  {
    uri: '/locations',
    method: RequestMethods.GET,
    auth: false,
    controller: testController
  },
  {
    uri: '/locations/add',
    method: RequestMethods.POST,
    auth: false,
    controller: testController
  }
]

export const profile: IApiRoute[] = [
  {
    uri: '/profile/view',
    method: RequestMethods.GET,
    auth: false,
    controller: testController
  },
  {
    uri: '/profile/list',
    method: RequestMethods.POST,
    auth: false,
    controller: testController
  }
]
