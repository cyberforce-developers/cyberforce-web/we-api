import Router from '../../dist/router'  // tested class
import IKeyAny from '@cyberforce/web-essentials/intf/IKeyAny'
import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'
import { expect } from 'chai'
import nodes from '../support/app/nodes'
import { jobs, profile, location } from '../support/app/routes'

const Api: IKeyAny = {
  Jobs: jobs,
  Location: location,
  Profile: profile
}

describe('Router', () => {

  it('returns all routes if `node` parameter is not set', () => {
    const allRoutes = Api.Jobs.concat(Api.Location)
    const router = new Router(nodes)
    const routes = router.getRoutes()
    expect(routes).eql(allRoutes)
  })

  it('returns specific api routes if `node` parameter is set', () => {
    const nodeParam = Object.keys(nodes)[1]
    let api1Routes: IApiRoute[][] = []
    nodes[nodeParam].forEach((api: IApiRoute[]) => {
      api1Routes = api1Routes.concat(api)
    })
    const router = new Router(nodes, nodeParam)
    const routes = router.getRoutes()
    expect(routes).eql(api1Routes)
  })

  it.skip('should not return duplicated paths if the request type is the same for both', () => {
    // On 22/07 we decided that preventing duplications in URI segments is not worth an effort.v
  })
})
