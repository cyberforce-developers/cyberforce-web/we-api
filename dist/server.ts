import * as express from 'express'
import { Server } from 'http'
import { IServerConstructor } from '../src/app/intf/IServer'
import IServer from '../src/app/intf/IServer'
import INodes from '../src/app/intf/INodes'
import IApiConfig from '../src/app/intf/IApiConfig'
import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'
import IApiConfigEnvironment from '../src/app/intf/IApiConfigEnvironment'

import Router from './router'

import Environments from '@cyberforce/web-essentials/props/Environments'
import RequestMethods from '@cyberforce/web-essentials/props/RequestMethods'
import { Logger } from '@cyberforce/web-essentials/modules/logger'
import LogLevels from '@cyberforce/web-essentials/props/LogLevels'
import LogSlugs from '../src/app/props/LogSlugs'
import bodyParser = require('body-parser')

class CoreServer implements IServer {

  private nodes: INodes

  private config: IApiConfig

  private node: string | undefined

  private environment: string

  constructor(args: IServerConstructor) {
    this.nodes = args.nodes
    this.config = args.config
    this.node = args.node
    this.environment = args.environment
  }

  public start(callback?: any): Server {
    const app = express()
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())
    const settings = this.getEnvironmentConfig()
    if (settings) {
      app.use((req: any, res: any, next: any) => {
        const { allowHeaders } = settings.server.headers
        res.header('Access-Control-Allow-Credentials', settings.server.headers.allowCredentials)
        res.header('Access-Control-Allow-Methods', settings.server.headers.allowMethods)
        if (allowHeaders) {
          res.header('Access-Control-Allow-Headers', allowHeaders)
        }
        if (this.allowRequestFromOrigin(req.hostname, settings)) {
          res.header('Access-Control-Allow-Origin', req.headers.origin)
        }
        next()
      })
      const router = new Router(this.nodes, this.node)
      const routes = router.getRoutes()
      this.bind(app, routes)
      return app.listen(settings.server.port, () => {
        if (this.environment !== Environments.testing) {
          const startupInfo = `Environment: "${this.environment}", Hostname: "${settings.server.hostname}", Port: "${settings.server.port}"`
          Logger.startup(`Application is listening.\n${startupInfo}`)
        }
        if (callback) {
          callback()
        }
      })
    } else {
      const slug = LogSlugs.server_start_no_config
      Logger.screen({
        level: LogLevels.error,
        slug,
        message: 'Server configuration not found in config'
      })
      throw new Error(slug)
    }
  }

  private allowRequestFromOrigin(hostname: string, environment: IApiConfigEnvironment): boolean {
    const { cors } = environment.server
    const hostnames: string[] = cors
    if (!cors.includes(environment.server.hostname)) {
      cors.push(environment.server.hostname)
    }
    return hostnames.includes(hostname)
  }

  private getEnvironmentConfig(): IApiConfigEnvironment | undefined {
    return (this.config.envs && this.config.envs.hasOwnProperty(this.environment))
      ? this.config.envs[this.environment]
      : undefined
  }

  private bind(app: express.Express, routes: IApiRoute[]): express.Express {
    if (routes.length > 0) {
      routes.forEach((route: IApiRoute) => {
        if (this.environment !== Environments.testing) {
          Logger.startup('+ ' + route.uri, true)
        }
        switch (route.method) {
          case RequestMethods.GET:
            app.route(route.uri).get(route.controller)
            break
          case RequestMethods.POST:
            app.route(route.uri).post(route.controller)
            break
          default:
            Logger.screen({
              level: LogLevels.error,
              slug: LogSlugs.server_bind_unknown_request,
              message: 'Unknown request type passed to the route'
            })
        }
      })
    } else {
      const slug = LogSlugs.server_bind_no_routes
      Logger.screen({
        level: LogLevels.error,
        slug,
        message: 'No API routes provided or `node` parameter is incorrect'
      })
      throw new Error(slug)
    }
    return app
  }
}

export default CoreServer
