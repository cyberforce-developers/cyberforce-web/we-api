import CoreServer from './server'
import nodes from '../src/api/nodes'
import config from '../src/config'
import { Logger } from '@cyberforce/web-essentials/modules/logger'
import LogLevels from '@cyberforce/web-essentials/props/LogLevels'
import LogSlugs from '../src/app/props/LogSlugs'

const node = process.env.node || undefined

if (node) {
  Logger.startup(`You are launching the '${node}' node...`)
} else {
  Logger.startup('You are launching all the nodes...')
}

const environment = process.env.NODE_ENV || 'local'

const coreServer = new CoreServer({nodes, node, config, environment})

const server = coreServer.start()

const sign = ['SIGINT', 'SIGTERM']
sign.forEach((sig: any) => {
  process.on(sig, () => {
    server.close()
    console.info()
    Logger.screen({
      level: LogLevels.warning,
      slug: LogSlugs.app_sigterm_server_killed,
      message: 'Server killed'
    })
    process.exit()
  })
})
