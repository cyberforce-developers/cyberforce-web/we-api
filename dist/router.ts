import IRouter from '../src/app/intf/IRouter'
import IApiRoute from '@cyberforce/web-essentials/intf/IApiRoute'
import INodes from '../src/app/intf/INodes'

class Router implements IRouter {

  private static getApiRoutes(nodeRoutes: IApiRoute[][]): IApiRoute[] {
    const routes: IApiRoute[] = []
    nodeRoutes.forEach((api: IApiRoute[]) => {
      api.forEach((route: IApiRoute) => {
        routes.push(route)
      })
    })
    return routes
  }

  private nodes: INodes

  private node: string | undefined

  constructor(nodes: INodes, node?: string) {
    this.nodes = nodes
    this.node = node
  }

  public getRoutes(): IApiRoute[] {
    return this.node
      ? this.nodes.hasOwnProperty(this.node)
        ? this.getNodeRoutes()
        : []
      : this.getAllRoutes()
  }

  public getAllRoutes(): IApiRoute[] {
    const nodeRoutes: IApiRoute[][] = this.nodes.all
    return Router.getApiRoutes(nodeRoutes)
  }

  public getNodeRoutes(): IApiRoute[] {
    const nodeRoutes: IApiRoute[][] = this.nodes[this.node || 'all']
    return Router.getApiRoutes(nodeRoutes)
  }

}

export default Router
