
# ITN Core
ITN Core is the main InTouch Networks web API application that provides microservices for the primary business domain.

## Installation

You should use be using Mac or Linux when working on this project. Windows is not supported. In order to start using the project follow the steps below.

1. Install `nvm` via: https://gist.github.com/d2s/372b5943bce17b964a79 . NodeJS and NPM versions used in the project are listed in `package.json` (engines).
2. Install `npm`. On Debian: `sudo apt-get install npm`
3. Install `git`. On Debian: `sudo apt-get install git`
4. Checkout the repository from: https://bitbucket.org/intouchnetworks/itn-core . Ideally: `git clone git@bitbucket.org:intouchnetworks/itn-core.git` . Don't use HTTPS cloning.
5. Set up access for NPM private package. Go to the project directory and run `NPM_TOKEN=xxx npm run setup`. Obtain a token (xxx) from other dev team members. 
6. Go to the project directory and install packages with `npm install`. 

## Start-up

The application is fully scalable in terms of microservices and their groups. 

### All services

All services are useful for performance measurement, local and development environments and automation tests. If you want to initialise all the services available please use the default command: `npm start`

### Specific APIs services

You can choose and define which services you want to launch. In order to do it you need to set-up or choose a Node. Nodes are lists of Services to be initialised. You can configure them in `src/api/nodes.ts`. Node named `all` is responsible for execution of `npm start`.

In order to launch a specific Node use: `node=<node_name> npm start` where `<node_name>` is a property key from the `nodes.ts` file. 

List of available API services is in `src/app/props/Api.ts`.

### Testing

In order to run a single time local tests (useful for example before _push_) use: `npm test`. This command will display also test coverage.

In most of cases you should work on the project in TDD manner, writing tests first. In such case it is convinient to run a test watcher. Depending on tests category you work on, you can run:
- `npm run test:integration` for all integration tests
- `npm run test:unit` for all unit tests

If you want to work on the specific test spec you can add `.only` to the particular `describe()` or add a file spec name to the commands above, assigning them to `SPEC` constant. For example: `SPEC=system npm run test:integration` to work on `system.spec.ts`


## Optional configuration

### VSCode settings

_Preferred path style for auto imports._
`non relative`

_Preferred quote style to use for quick fixes._
`single`

_Insert spaces when pressing <Tab>_
`yes`

_Number of spaces inserted when pressing <Tab>_
`2`