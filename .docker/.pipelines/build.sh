set -e

# Git LFS pull (pop to cache)
git lfs pull

if [ -f 'package.json' ]; then
  NPM_TOKEN=${NPM_TOKEN} npm run preinstall
  npm install
fi

### Image build
eval $(aws ecr get-login --region ${AWS_DEFAULT_REGION} --no-include-email)

# Build image
dockerfile=".docker/node/Dockerfile"

export IMAGE_NAME=755399320166.dkr.ecr.eu-west-1.amazonaws.com/${DOCKER_APP_NAME}:${BITBUCKET_BRANCH}-${BITBUCKET_COMMIT}
docker build --build-arg DOCKER_ENV=${BITBUCKET_BRANCH} --build-arg DOCKER_APP_NAME=${DOCKER_APP_NAME} --file ${dockerfile} -t $IMAGE_NAME .
echo $IMAGE_NAME
docker push $IMAGE_NAME
